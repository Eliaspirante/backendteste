# README #

### This is only a simple backend project ###

#### Run the sql file ( sql_database/create_db.sql ) before running the project ####

If you want to get the bower dependencies (not necessary):

* bower install

This project uses CodeIgniter 3.0 and has only two 'views' and some routes for login control and POST Data from project 'Simple Landing Teste'.

The project contains others sub projects, they are:

* MY_Model ( https://github.com/avenirer/CodeIgniter-MY_Model ): for generic DAO ;
* Ion_auth ( http://benedmunds.com/ion_auth ): for login/permission control;

Structure of the backend teste project

```
#!php

.
├── application
│   ├── cache
│   ├── config
│   ├── controllers
│   ├── core
│   ├── helpers
│   ├── hooks
│   ├── language
│   │   └── english
│   ├── libraries
│   ├── logs
│   ├── models
│   ├── third_party
│   └── views
│       ├── login
│       ├── registers
│       └── template
├── assets
│   ├── css
│   └── js
├── sql_database
└── system
```


If you want to see/modify some functions, go to the files:
* Routes: "config/routes.php";
* Database: "config/database.php";
* Autoload: "config/autoload.php";
* Ion_auth: "config/ion_auth.php";
* MY_Model: "core/MY_Model.php";

The base views files, for the project, are located at "views/template", and contains:

```
#!php

.
├── footer.php
├── header.php
└── sidebar.php
```


These files are used in other views files, like "registers/list.php" or "login/view.php".