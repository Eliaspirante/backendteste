<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_controller extends CI_Controller {

    public function index()
    {
        if($_POST){
            $error = false;

            if(!array_key_exists("name", $_POST) && !isset($_POST['name'])){
                $error = true;
            }

            if(!array_key_exists("email", $_POST) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                $error = true;
            }

            if(!array_key_exists("birthdate", $_POST) && !isset($_POST['name'])){
                $error = true;
            }

            if(!array_key_exists("genre", $_POST)){
                $error = true;
            }

            if($error) {
                echo json_encode(array("code" => 500, "message" => "Cannot save data"));
            }else{

                //Initializing the model, without alias
                $this->load->model("Register_model");

                //Verifying if emails is already added
                $register_exists = $this->Register_model->where("email", $_POST['email'])->get_all();

                // If register exists, only respond success
                if($register_exists){
                    echo json_encode(array("code" => 200, "message" => "Already exists in the base."));
                }else {

                    // Mount the
                    $register = array(
                        "name" => $_POST['name'],
                        "email" => $_POST['email'],
                        "genre" => $_POST['genre'],
                        "birthdate" => $_POST['birthdate'],
                    );

                    $id = $this->Register_model->insert($register);

                    //Insert error
                    if (!$id) {
                        echo json_encode(array("code" => 500, "message" => "Error during save data"));
                    } else { //Insert ok, and we have the id from the last inserted data
                        echo json_encode(array("code" => 200, "register_id" => $id));
                    }
                }
            }
        }
    }
}
