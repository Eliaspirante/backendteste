<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        // If no user are logged in, so redirect do login view
        if(!$this->ion_auth->logged_in()){
            redirect("index.php/entrar","refresh");
        }

        $this->load->model("Register_model");
    }

    public function index()
    {
        //Because only for tests, no limit has been set
        $data["registers"] = $this->Register_model->get_all();

        $this->load->view('registers/list', $data);
    }
}
