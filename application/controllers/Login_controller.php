<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller {

    public function index()
    {
        $this->load->view('login/view');
    }

    public function doLogin(){
        if($_POST){
            if ($this->ion_auth->login($this->input->post('user_login'), $this->input->post('user_pass')))
            {
                //if the login is successful
                //redirect them back to the dashboard home page
                redirect('index.php/dashboard', 'refresh');
            }
            else
            {
                //if the login was un-successful
                //redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('/', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        }
    }

    public function doLogout(){
        $this->ion_auth->logout();

        $this->session->unset_userdata(array('logged' => '0'));

        redirect('/', 'refresh');
    }
}
