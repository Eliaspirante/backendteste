<?php
class Register_model extends MY_Model
{
    public $table = 'registers'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key

    public function __construct()
    {
        parent::__construct();
    }
}