<?php
    $this->load->view("template/header");
?>

<?php
$this->load->view("template/sidebar");
?>

<div class="row text-center">
    <form action="<?php echo base_url(); ?>index.php/logar" method="post">
        <p class="col-md-12">
            <div class="col-md-3"></div>
            <label class="col-md-2" for="user_login">Usuário:</label>
            <input class="col-md-3" id="user_login" name="user_login" type="text" required/>
        </p>
        <p class="col-md-12">
            <div class="col-md-3"></div>
            <label class="col-md-2" for="user_pass">Senha:</label>
            <input class="col-md-3" id="user_pass" name="user_pass" type="password" required/>
        </p>
        <div class="col-md-3"></div>
        <p class="col-md-12">
            <div class="col-md-4"></div>
            <input class="col-md-4" type="submit" value="Logar"/>
        </p>
        <div class="col-md-3"></div>
    </form>
</div>
<?php
$this->load->view("template/footer");
?>
