<?php
$this->load->view("template/header");
?>

<?php
$this->load->view("template/sidebar");
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.dataTables.min.css">

<table id="table_id" class="display">
    <thead>
    <tr>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Data de Nascimento</th>
        <th>Género</th>
        <th>Data Cadastro</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($registers) && $registers != null): ?>
        <?php foreach($registers as $register): ?>
            <tr>
                <td><?php echo $register->name;?></td>
                <td><?php echo $register->email;?></td>
                <td><?php echo $register->birthdate;?></td>
                <td><?php echo $register->genre;?></td>
                <td><?php echo $register->created_at;?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>

<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () {
        $('#table_id').DataTable();
    } );
</script>
<?php
$this->load->view("template/footer");
?>
