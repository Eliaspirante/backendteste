<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Backend API</a>
            </div>
            <?php if($this->ion_auth->logged_in()): ?>
                <p>
                    <div class="pull-right">
                        <a href="<?php echo base_url();?>index.php/deslogar">Sair</a>
                    </div>
                </p>
            <?php endif; ?>
        </div>
    </nav>
    <br/>
    <br/>
    <br/>
    <div class="container">